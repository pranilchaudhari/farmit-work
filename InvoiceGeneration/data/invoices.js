const fs = require("fs");
const PDFDocument = require("pdfkit");

let exportedMethods = {
    async createInvoicePDF(newInvoiceData) {
        let doc = new PDFDocument({ size: "A4", margin: 50 });
        console.log(newInvoiceData);
        // Logic to generate Invoive PDF
        generateHeader(doc);
  generateCustomerInformation(doc, newInvoiceData);
  generateInvoiceTable(doc, newInvoiceData);
  generateFooter(doc);

  doc.end();
  const path ="File_"+(newInvoiceData.shipping.name).trim()+".pdf"
  doc.pipe(fs.createWriteStream(path));
        
        return await path;
    }
};

module.exports = exportedMethods;

function generateHeader(doc) {
  doc
    .fontSize(5)
    .text("1986", 240, 50)  
    .fontSize(20)
    .text("SilverPeak", 250, 57)
    .fontSize(5)
    .text("2013", 320, 50)
//    .text("ACME Inc.", 200, 50, { align: "right" })
//    .text("123 Main Street", 200, 65, { align: "right" })
//    .text("New York, NY, 10025", 200, 80, { align: "right" })
    .moveDown();
}

function generateCustomerInformation(doc, newInvoiceData) {
  
    generateHr(doc, 155);
    doc
    .fillColor("#444444")
    .fontSize(20)
    .text("Invoice", 50, 160,{ align: "LEFT" });

  generateHr(doc, 185);

  const customerInformationTop = 200;

  doc
    .fontSize(10)
    .text("Invoice Number:", 50, customerInformationTop,{ align: "right" })
    .font("Helvetica-Bold")
    .text(newInvoiceData.invoice_nr, 150, customerInformationTop)
    .font("Helvetica")
    .text("Invoice Date:", 50, customerInformationTop + 15)
    .text(formatDate(new Date()), 150, customerInformationTop + 15)
    .text("Balance Due:", 50, customerInformationTop + 30)
    .text(
      formatCurrency(newInvoiceData.subtotal - newInvoiceData.paid),
      150,
      customerInformationTop + 30
    )

    .font("Helvetica-Bold")
    .text(newInvoiceData.shipping.name, 300, customerInformationTop)
    .font("Helvetica")
    .text(newInvoiceData.shipping.address, 300, customerInformationTop + 15)
    .text(
      newInvoiceData.shipping.city +
        ", " +
        newInvoiceData.shipping.state +
        ", " +
        newInvoiceData.shipping.country,
      300,
      customerInformationTop + 30
    )
    .moveDown();

  generateHr(doc, 252);
}

function generateInvoiceTable(doc, newInvoiceData) {
  let i;
  const invoiceTableTop = 330;

  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    invoiceTableTop,
    "Item",
    "Description",
    "Unit Cost",
    "Quantity",
    "Line Total"
  );
  generateHr(doc, invoiceTableTop + 20);
  doc.font("Helvetica");

  for (i = 0; i < newInvoiceData.items.length; i++) {
    const item = newInvoiceData.items[i];
    const position = invoiceTableTop + (i + 1) * 30;
    generateTableRow(
      doc,
      position,
      item.item,
      item.description,
      formatCurrency(item.amount / item.quantity),
      item.quantity,
      formatCurrency(item.amount)
    );

    generateHr(doc, position + 20);
  }

  const subtotalPosition = invoiceTableTop + (i + 1) * 30;
  generateTableRow(
    doc,
    subtotalPosition,
    "",
    "",
    "Subtotal",
    "",
    formatCurrency(newInvoiceData.subtotal)
  );

  const paidToDatePosition = subtotalPosition + 20;
  generateTableRow(
    doc,
    paidToDatePosition,
    "",
    "",
    "Paid To Date",
    "",
    formatCurrency(newInvoiceData.paid)
  );

  const duePosition = paidToDatePosition + 25;
  doc.font("Helvetica-Bold");
  generateTableRow(
    doc,
    duePosition,
    "",
    "",
    "Balance Due",
    "",
    formatCurrency(newInvoiceData.subtotal - newInvoiceData.paid)
  );
  doc.font("Helvetica");
}

function generateFooter(doc) {
  doc
    .fontSize(10)
    .text(
      "Payment is due within 15 days. Thank you for your business.",
      50,
      780,
      { align: "center", width: 500 }
    );
}

function generateTableRow(
  doc,
  y,
  item,
  description,
  unitCost,
  quantity,
  lineTotal
) {
  doc
    .fontSize(10)
    //.text(item, 50, y)
    .text(description, 50, y,)
    .text(unitCost, 280, y, { width: 90, align: "right" })
    .text(quantity, 370, y, { width: 90, align: "right" })
    .text(lineTotal, 0, y, { align: "right" });
}

function generateHr(doc, y) {
  doc
    .strokeColor("#aaaaaa")
    .lineWidth(1)
    .moveTo(50, y)
    .lineTo(550, y)
    .stroke();
}

function formatCurrency(cents) {
  return "$" + (cents / 100).toFixed(2);
}

function formatDate(date) {
  const day = date.getDate();
  const month = date.getMonth() + 1;
  const year = date.getFullYear();

  return year + "/" + month + "/" + day;
}

