const invoiceRoutes = require("./invoices");

let constructorMethod = app => {
  app.use("/invoice", invoiceRoutes);
};

module.exports = {
  invoices: require("./invoices")
};
