const express = require("express");
const router = express.Router();
const data = require("../data");
const invoiceData = data.invoices;
try {

  router.post("/create", async (req, res) => {
    console.log(req.body);
    let newInvoiceData = req.body;
    

    /********* Below if block for Requet Data validation **********/

    /*if (errors.length > 0) {
      console.log("hi1");
      res.render("invoices/new", {
        errors: errors,
        hasErrors: true
      });
      return;
    }*/

    try {
      const newInvoicePath = await invoiceData.createInvoicePDF(newInvoiceData);
      console.log(newInvoicePath);
      res.status(200).json(newInvoicePath);
    } catch (e) {
      console.log(e);
      res.status(500).json({
        error: e
      });
    }
  });

  module.exports = router;
} catch (e) {
  throw "Problem in invoices routes";
}