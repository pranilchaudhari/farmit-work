const invoiceRoutes = require("./invoice");

const constructorMethod = app => {
  app.use("/invoices", invoiceRoutes);

};

module.exports = constructorMethod;