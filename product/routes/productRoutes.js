const express = require("express");
const router = express.Router();
const data = require("../data");
const productData = data.productData;
try {
  router.post("/create", async (req, res) => {
    console.log("you are in router");
    let rawProductData = req.body;
    let errors = [];

/*************** Request filtering as depending upon manditory fields *************/
/*    if (!rawProductData.title) {
      errors.push("No title provided");
    }

    if (!rawProductData.body) {
      errors.push("No body provided");
    }

    if (!rawProductData.userId) {
      errors.push("No goaler selected");
    }
    // console.log(errors.length);
    if (errors.length > 0) {
      console.log("hi1");
      res.render("goals/new", {
        errors: errors,
        hasErrors: true,
        goal: rawProductData
      });
      return;
    }
*/
    try {
      const newProduct = await productData.addProduct(rawProductData);
      //console.log(newProduct);
      res.status(200).json(newProduct);
      // res.redirect(`/goals/${newProduct._id}`);
    } catch (e) {
      console.log(e);
      res.status(500).json({
        error: e
      });
    }
  });


  module.exports = router;
} catch (e) {
  throw "Problem in goals routes";
}