const goalRoutes = require("./productRoutes");
//const path = require("path");

const constructorMethod = app => {
  app.use("/product", goalRoutes);
  app.use("*", (req, res) => {
    res.redirect("/product");
  });
};

module.exports = constructorMethod;