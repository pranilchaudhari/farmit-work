const mongoCollections = require("../config/mongoCollections");
const product = mongoCollections.product;
//const uuid = require("node-uuid");

let exportedMethods  = {
    async addProduct(rawProductData) {
        console.log("you are in data");
        const productCollection = await product();
        const maxIdProduct = await productCollection.find({}).sort({productId:-1}).limit(1).toArray();
        var maxProductId = ++maxIdProduct[0].productId;
        console.log(maxProductId);

        const insertProductData = {
            productId:maxProductId,
            Cname:rawProductData.Cname,
            address1:rawProductData.address1,
            address2:rawProductData.address2,
            city:rawProductData.city,
            state:rawProductData.state,
            pincode:rawProductData.pincode,
            contactNo:rawProductData.contactNo,
            emaill:rawProductData.emaill,
            website:rawProductData.website,
            TDS_No:rawProductData.TDS_No,
            CST_No:rawProductData.CST_No,
            VAT_No:rawProductData.VAT_No,
            GST_No:rawProductData.GST_No,
            PAN_No:rawProductData.PAN_No,
            PT_No:rawProductData.PT_No,
            LBT_No:rawProductData.LBT_No,
            createdBy:rawProductData.createdBy,
            createdDate:new Date(),
            modifiedBy:rawProductData.modifiedBy,
            modifiedDate:new Date()
        };

        //const userThatgoaled = await users.getUserById(userId);
        const insertInfo = await productCollection.insertOne(rawProductData);
        if (insertInfo.insertedCount === 0) throw "Could not add post";

        return await insertInfo;
    }
};

module.exports = exportedMethods;