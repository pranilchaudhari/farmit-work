const productRoutes = require("./productData");

let constructorMethod = app => {
  app.use("/product", productRoutes);
};

module.exports = {
  productData: require("./productData")
};