const mongoCollections = require("../config/mongoCollections");
const client = mongoCollections.client;
//const uuid = require("node-uuid");

let exportedMethods  = {
    async addClient(rawClientData) {
        console.log("you are in data");
        const clientCollection = await client();
        const maxIdClient = await clientCollection.find({}).sort({clientId:-1}).limit(1).toArray();
        var maxClientId = ++maxIdClient[0].clientId;
        console.log(maxClientId);

        const insertClientData = {
            clientId:maxClientId,
            Cname:rawClientData.Cname,
            address1:rawClientData.address1,
            address2:rawClientData.address2,
            city:rawClientData.city,
            state:rawClientData.state,
            pincode:rawClientData.pincode,
            contactNo:rawClientData.contactNo,
            emaill:rawClientData.emaill,
            website:rawClientData.website,
            TDS_No:rawClientData.TDS_No,
            CST_No:rawClientData.CST_No,
            VAT_No:rawClientData.VAT_No,
            GST_No:rawClientData.GST_No,
            PAN_No:rawClientData.PAN_No,
            PT_No:rawClientData.PT_No,
            LBT_No:rawClientData.LBT_No,
            createdBy:rawClientData.createdBy,
            createdDate:new Date(),
            modifiedBy:rawClientData.modifiedBy,
            modifiedDate:new Date()
        };

        const insertInfo = await clientCollection.insertOne(insertClientData);
        if (insertInfo.insertedCount === 0) throw "Could not add post";

        return await insertInfo;
    },

    async getclientById(id) {
        console.log("you are in get client data with id="+id);
        if (!id) throw "No client found";
        const clientCollection = await client();
        const getClient = await clientCollection.findOne({clientId:parseInt(id)});
        if (client === null) throw "No client found";
        return getClient;
    },

    updateclient(id, updatedclient) {
        return clients().then(clientCollection => {
            let updatedclientData = {};
            var l_objClient = this.getclientById(id);
            if (updatedclient.tags) {
                updatedclientData.tags = updatedclient.tags;
            }

            if (updatedclient.title) {
                updatedclientData.name = updatedclient.title;
            }

            if (updatedclient.gamount) {
                updatedclientData.gamount = updatedclient.gamount;
            } else {
                updatedclientData.gamount = l_objClient.gamount;
            }
            if (updatedclient.gfulfilment) {
                updatedclientData.gfulfilment = l_objClient.gfulfilment;
                updatedclientData.gfulfilment = updatedclientData.gdescription + updatedclient.gfulfilment;
            }
            if (updatedclient.gpriority) {
                updatedclientData.gpriority = updatedclient.gpriority;
            }
            updatedclientData.gmodifiedOn = new Date();
            if (updatedclientData.gfulfilment > updatedclientData.gamount) {
                var l_numCarryOver = updatedclientData.gfulfilment - updatedclientData.gamount;
                updatedclientData.gstatus = 1;
                updatedclientData.gendDate = new Date();
                var l_objCarryOver = this.getclientByPriority(l_objClient.gpriority++);
                this.updateclient(l_objCarryOver._id, {
                    "gfulfilment": l_numCarryOver
                });
            }

            let updateCommand = {
                $set: updatedclientData
            };

            return clientCollection
                .updateOne({
                    _id: id
                }, updateCommand)
                .then(result => {
                    return this.getclientById(id);
                });
        });
    }
};

module.exports = exportedMethods;