const goalRoutes = require("./clientRoutes");
//const path = require("path");

const constructorMethod = app => {
  app.use("/client", goalRoutes);
  app.use("*", (req, res) => {
    res.redirect("/client");
  });
};

module.exports = constructorMethod;