const express = require("express");
const router = express.Router();
const data = require("../data");
const clientData = data.clientData;
try {
  router.post("/create", async (req, res) => {
    console.log("you are in router");
    let rawClientData = req.body;
    let errors = [];

    try {
      const newClient = await clientData.addClient(rawClientData);
      res.status(200).json(newClient);
    } catch (e) {
      console.log(e);
      res.status(500).json({
        error: e
      });
    }
  });

  router.get("/:id", async (req, res) => {
    console.log("you are in get client router");
    const client = await clientData.getclientById(req.params.id);
    res.status(200).json({
      client: client
    });
  });

  router.put("/:id", (req, res) => {
    let updatedData = req.body;

    let getclient = clientData.getclientById(req.params.id);

    getclient
      .then(() => {
        return clientData
          .updateclient(req.params.id, updatedData)
          .then(updatedclient => {
            res.json(updatedclient);
          })
          .catch(e => {
            res.status(500).json({
              error: e
            });
          });
      })
      .catch(() => {
        res.status(404).json({
          error: "client not found"
        });
      });
  });

  module.exports = router;
} catch (e) {
  throw "Problem in goals routes";
}